per il progetto ho usato le ultime versioni di node e npm.

procedura:   
scaricare progetto   
entrare nella cartella principale e lanciare i seguenti comandi
>npm i   
>npm start    
>npm run serverino   

prire la pagina all'indirizzo http://localhost:4200

Sono presenti 3 pagine principali
- Home (ci sono grafici messi solo per bellezza)
- Coin (è la sezione realizzata a fronte delle richieste)
- Asset (sezione realizzata per motivi di sviluppo)

non ho cancellato le sezioni in + giusto per fare numero nel menù.

## COIN
nella parte superiore della pagina sono presenti due campi, 
### Campo a sinistra 

il campo a sinistra serve ad eseguire una ricerca sulle monete inserite, digitando all'interno del campo _limit=5&_page=3 e premendo invio il sistema richiede la terza pagina paginando per 5 elementi.

#### Filter

Use `.` to access deep properties

```
GET /posts?title=json-server&author=typicode
GET /posts?id=1&id=2
GET /comments?author.name=typicode
```

#### Paginate

Use `_page` and optionally `_limit` to paginate returned data.

In the `Link` header you'll get `first`, `prev`, `next` and `last` links.


```
GET /posts?_page=7
GET /posts?_page=7&_limit=20
```

_10 items are returned by default_

#### Sort

Add `_sort` and `_order` (ascending order by default)

```
GET /posts?_sort=views&_order=asc
GET /posts/1/comments?_sort=votes&_order=asc
```

For multiple fields, use the following format:

```
GET /posts?_sort=user,views&_order=desc,asc
```

#### Slice

Add `_start` and `_end` or `_limit` (an `X-Total-Count` header is included in the response)

```
GET /posts?_start=20&_end=30
GET /posts/1/comments?_start=20&_end=30
GET /posts/1/comments?_start=20&_limit=10
```

_Works exactly as [Array.slice](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/slice) (i.e. `_start` is inclusive and `_end` exclusive)_

#### Operators

Add `_gte` or `_lte` for getting a range

```
GET /posts?views_gte=10&views_lte=20
```

Add `_ne` to exclude a value

```
GET /posts?id_ne=1
```

Add `_like` to filter (RegExp supported)

```
GET /posts?title_like=server
```

#### Full-text search

Add `q`

```
GET /posts?q=internet
```

### Campo a destra

Il campo a destra serve a selezionare un asset e inserirlo nella tabella sottostante.  
Il completamento della lista avviene a fronte di chiamate asincrone, con un debounce di 250ms, non ci sono limiti minimi e massimi di caratteri.
Dopo aver selezionato un elemento, bisogna premere il pulsante per aggiungere la moneta.
Il pulsante si abilita solo se esiste un asset selezionato.
Gli asset già aggiunti sono disabilitati nella lista, non si possono selezionare nuovamente.


### Dettaglio
iconcina con occhio, nella parte superiore ho messo una serie di pulsanti per selezionare il periodo, i periodi indicati in documentazione non li ho trovati nella documentazione dei servizi.

### Modifica
Iconcina con matita, apre popUp con il forma per ma modifica, un solo campo è abilitato, mi serviva il form e il json per motivi di sviluppo.

### Elimina
Cestino


