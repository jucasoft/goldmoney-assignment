import {Component, HostListener, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {RootStoreState, SlideMenuStoreActions, SlideMenuStoreSelectors} from '@root-store/index';
import {Observable} from 'rxjs';
import {NgSelectConfig} from '@ng-select/ng-select';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private readonly store$: Store<RootStoreState.State>, private config: NgSelectConfig) {
    this.config.notFoundText = 'No assets found';
  }

  open$: Observable<boolean>;

  @HostListener('document:keydown.escape', ['$event'])
  onMouseup(event: KeyboardEvent) {

  }

  onClickOutside($event, open, elements) {
    console.log('AppComponent.onClickOutside(arguments)');
    if (open && elements.offsetLeft === 0) {
      this.store$.dispatch(SlideMenuStoreActions.Open({open: !open}));
    }
  }

  ngOnInit() {
    this.open$ = this.store$.select(SlideMenuStoreSelectors.selectOpen);
  }

}
