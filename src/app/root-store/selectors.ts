import {HistoryStoreSelectors} from '@root-store/history-store';
import {PriceStoreSelectors} from '@root-store/price-store';
import {AssetStoreSelectors} from '@root-store/asset-store';
import {CoinStoreSelectors} from '@root-store/coin-store';
import {createSelectorFactory, defaultMemoize} from '@ngrx/store';

const customMemoizer = (aFn) => defaultMemoize(aFn, (a: any, b: any) => a === b);

export const selectError =
  createSelectorFactory(customMemoizer)(
HistoryStoreSelectors.selectError,
    PriceStoreSelectors.selectError,
    AssetStoreSelectors.selectError,
    CoinStoreSelectors.selectError,
    (...args: string[]) => {
      // console.log('selectError.args', args);
      return args.join('');
    }
  );

export const selectIsLoading =
  createSelectorFactory(customMemoizer)(
HistoryStoreSelectors.selectIsLoading,
    AssetStoreSelectors.selectIsLoading,
    CoinStoreSelectors.selectIsLoading,
    (...args: boolean[]) => {
      // console.log('selectIsLoading.args', args);
      return args.find((value => value));
    }
  );

