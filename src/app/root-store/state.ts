import {HistoryStoreState} from '@root-store/history-store';
import {PriceStoreState} from '@root-store/price-store';
import {AssetStoreState} from '@root-store/asset-store';
import {CoinStoreState} from '@root-store/coin-store';
import {SlideMenuStoreState} from '@root-store/slide-menu-store';

export interface State {
history:HistoryStoreState.State;
  price: PriceStoreState.State;
  asset: AssetStoreState.State;
  coin: CoinStoreState.State;
  slide_menu: SlideMenuStoreState.State;
}
