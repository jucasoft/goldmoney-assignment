import {createCrudEntityAdapter, EntityCrudAdapter, EntityCrudState} from 'ngrx-entity-crud';
import {Price} from '@models/vo/price';

export const adapter: EntityCrudAdapter<Price> = createCrudEntityAdapter<Price>({
	// selectId: model => Price.selectId(model),
});

export interface State extends EntityCrudState<Price> {
};

export const initialState: State = adapter.getInitialCrudState();
