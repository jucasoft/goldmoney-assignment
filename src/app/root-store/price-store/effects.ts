import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {PriceService} from '@services/price.service';
import {Price} from '@models/vo/price';
import {map, tap} from 'rxjs/operators';
import {actions} from './actions';

@Injectable()
export class PriceStoreEffects {
  constructor(
    private readonly actions$: Actions,
    private readonly service: PriceService
  ) {

  }

  toPrices(value: any): Price[] {

    const result: Price[] = [];
    const keys = Object.keys(value);

    keys.forEach(key => {
      result.push({id: key, value: value[key]});
    });

    return result;
  }

  @Effect()
  liveCreate$ = this.service.message$.pipe(
    tap(message => console.log('message', message)),
    map(contact => {
      return actions.SearchSuccess({items: this.toPrices(contact)});
    })
  );

}
