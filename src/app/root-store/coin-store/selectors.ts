import {createFeatureSelector, createSelector, MemoizedSelector} from '@ngrx/store';

import {adapter, State} from './state';
import {Names} from './names';
import {PriceStoreSelectors} from '@root-store/price-store';
import {Coin} from '@models/vo/coin';
import {Price} from '@models/vo/price';

export const selectState: MemoizedSelector<object, State> = createFeatureSelector<State>(Names.NAME);
export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
  selectItemSelected,
  selectItemsSelected,
  selectLastCriteria,
  selectError,
  selectIsLoading,
  selectIsLoaded,
  selectFilters,
  selectFilteredItems
} = adapter.getCrudSelectors(selectState);


export const selectAllExt: MemoizedSelector<object, Coin[]> = createSelector(
  selectAll,
  PriceStoreSelectors.selectEntities,
  (items, priceEntities) => {
    items.forEach((item, i, list) => {
      const price: Price = priceEntities[item.id];
      if (!!price) {
        list[i] = {...item, ...{priceUsd: price.value}};
      }
    });
    return [...items];
  }
);
