import {createFeatureSelector, createSelector, MemoizedSelector} from '@ngrx/store';

import {adapter, State} from './state';
import {Names} from './names';
import {CoinStoreSelectors} from '@root-store/coin-store';
import {Asset} from '@models/vo/asset';

export const selectState: MemoizedSelector<object, State> = createFeatureSelector<State>(Names.NAME);
export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
  selectItemSelected,
  selectItemsSelected,
  selectLastCriteria,
  selectError,
  selectIsLoading,
  selectIsLoaded,
  selectFilters,
  selectFilteredItems
} = adapter.getCrudSelectors(selectState);

export const selectAllExt: MemoizedSelector<object, Asset[]> = createSelector(
  selectAll,
  CoinStoreSelectors.selectEntities,
  (assets, coinEntities) => {
    console.log('coinEntities', coinEntities);
    assets.forEach((item, i, list) => {
      const disabled = !!coinEntities[item.id];
      if (disabled) {
        console.log('disabilito');
        list[i] = {...item, ...{disabled}};
      }
    });
    console.log('assets', assets);
    return assets;
  }
);
