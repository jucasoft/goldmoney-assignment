import {createCrudEntityAdapter, EntityCrudAdapter, EntityCrudState} from 'ngrx-entity-crud';
import {Asset} from '@models/vo/asset';

export const adapter: EntityCrudAdapter<Asset> = createCrudEntityAdapter<Asset>({
	selectId: model => Asset.selectId(model),
});

export interface State extends EntityCrudState<Asset> {
};

export const initialState: State = adapter.getInitialCrudState();
