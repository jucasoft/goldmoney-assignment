import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {Observable, of} from 'rxjs';
import {Action} from '@ngrx/store';
import {catchError, map, switchMap} from 'rxjs/operators';
import * as actions from './actions';
import {ICriteria, Response} from 'ngrx-entity-crud';
import {History} from '@models/vo/history';
import {HistoryService} from '@services/history.service';


@Injectable()
export class HistoryStoreEffects {
    constructor(private readonly actions$: Actions, private readonly service: HistoryService) {
    }

    @Effect()
    searchRequestEffect$: Observable<Action> = this.actions$.pipe(
      ofType(actions.SearchRequest),
      switchMap(criteria => this.service.search(criteria).pipe(
        map((response: Response<History[]>) => ({response, criteria}))
      )),
      switchMap(({response, criteria}: { response: any, criteria: ICriteria }) => {
          const result: Action[] = [];
          if (response.hasError) {
            result.push(actions.SearchFailure({error: response.message}));
            if (criteria.onFault) {
              result.push(...criteria.onFault);
            }
          } else {
            result.push(actions.SearchSuccess({items: response.data}));
            result.push(actions.Filters({filters: {}}));
            if (criteria.onResult) {
              result.push(...criteria.onResult);
            }
          }
          return result;
        }
      ),
      catchError(error => {
          return of(actions.SearchFailure({error}));
        }
      ),
    );


}
