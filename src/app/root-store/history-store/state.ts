import {createCrudEntityAdapter, EntityCrudAdapter, EntityCrudState} from 'ngrx-entity-crud';
import {History} from '@models/vo/history';

export const adapter: EntityCrudAdapter<History> = createCrudEntityAdapter<History>({
	selectId: model => History.selectId(model),
});

export interface State extends EntityCrudState<History> {
};

export const initialState: State = adapter.getInitialCrudState();
