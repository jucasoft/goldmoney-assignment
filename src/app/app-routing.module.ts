import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';


const routes: Routes = [
  {path: '', redirectTo: 'coin', pathMatch: 'full'},
  {path: 'home', loadChildren: () => import('./main/views/home/home.module').then(m => m.HomeModule)},
  {path: 'coin', loadChildren: () => import('./main/views/coin/coin.module').then(m => m.CoinModule)},
  {path: 'asset', loadChildren: () => import('./main/views/asset/asset.module').then(m => m.AssetModule)},
  {path: 'price', loadChildren: () => import('./main/views/price/price.module').then(m => m.PriceModule)},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: false})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
