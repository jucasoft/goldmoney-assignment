import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PriceEditComponent} from './price-edit/price-edit.component';
import {PriceMainComponent} from './price-main/price-main.component';
import {PriceListComponent} from './price-list/price-list.component';
import {PriceRoutingModule} from './price-routing.module';
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {SearchComponent} from '@components/search/search.component';
import {PipesModule} from '@core/pipe/pipes.module';
//testaaa
@NgModule({
  declarations: [
    PriceEditComponent,
    PriceMainComponent,
    PriceListComponent,
    SearchComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PriceRoutingModule,
    TableModule,
    DialogModule,
    ButtonModule,
    InputTextModule,
    PipesModule
  ],
  providers: [],
  entryComponents: []
})
export class PriceModule {
}
