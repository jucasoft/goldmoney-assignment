import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {PriceStoreActions, PriceStoreSelectors, RootStoreState} from '@root-store/index';
import {Observable} from 'rxjs';
import {Price} from '@models/vo/price';
import {RouterStoreActions} from '@root-store/router-store/index';
import {tap} from 'rxjs/operators';
import {ConfirmationService} from 'primeng/api';
import {PopUpData} from '@root-store/router-store/pop-up-base.component';

@Component({
  selector: 'app-price-list',
  templateUrl: `price-list.component.html`,
  styles: [``],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PriceListComponent implements OnInit {


  collection$: Observable<Price[]>;
  cols: any;

  constructor(private store$: Store<RootStoreState.State>,
              private confirmationService: ConfirmationService) {
    console.log('PriceListComponent.constructor()');
  }

  ngOnInit() {
    console.log('PriceListComponent.ngOnInit()');

    this.collection$ = this.store$.select(
      PriceStoreSelectors.selectAll
    ).pipe(
      tap(values => {
        if (values && values.length > 0) {
          this.cols = Object.keys(values[0]);
        }
      })
    );

    this.store$.dispatch(
      PriceStoreActions.SearchRequest({queryParams: {}})
    );

  }

  onEdit(item) {
    console.log('PriceListComponent.onEdit()');

    const state: PopUpData<Price> = {
      item,
      props: {title: 'Edit Price', route: 'price'}
    };

    // apro la popUP
    this.store$.dispatch(RouterStoreActions.RouterGo({
      path: ['price', {outlets: {popUp: ['edit']}}],
      extras: {state}
    }));

  }

  onCopy(value) {
    console.log('PriceListComponent.onCopy()');

    const item = {...{}, ...value, ...{id: null}};
    const state: PopUpData<Price> = {
      item,
      props: {title: 'Copy Price', route: 'price'}
    };

    this.store$.dispatch(RouterStoreActions.RouterGo({
      path: ['price', {outlets: {popUp: ['edit']}}],
      extras: {state}
    }));

  }

  onDelete(item) {

    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.store$.dispatch(PriceStoreActions.DeleteRequest({item}));
      }
    });

  }

}
