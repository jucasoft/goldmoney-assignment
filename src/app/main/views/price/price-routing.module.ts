import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PriceMainComponent} from './price-main/price-main.component';
import {PriceEditComponent} from '@views/price/price-edit/price-edit.component';

const routes: Routes = [
  {
    path: 'main',
    component: PriceMainComponent,
    pathMatch: 'full'
  },
  {
    path: 'edit',
    component: PriceEditComponent,
    outlet: 'popUp',
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'main',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PriceRoutingModule {
}
