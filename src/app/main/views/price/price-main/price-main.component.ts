import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {PriceStoreActions, RootStoreState} from '@root-store/index';
import {Actions} from 'ngrx-entity-crud';
import {Price} from '@models/vo/price';

@Component({
  selector: 'app-price-main',
  templateUrl: 'price-main.component.html',
  styles: []
})
export class PriceMainComponent implements OnInit {

  constructor(private readonly store$: Store<RootStoreState.State>) {
  }

  actions: Actions<Price> = PriceStoreActions.actions;

  ngOnInit() {
  }
}
