import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {CoinStoreActions, RootStoreState} from '@root-store/index';
import {Actions} from 'ngrx-entity-crud';
import {Coin} from '@models/vo/coin';
import {Asset} from '@models/vo/asset';

@Component({
  selector: 'app-coin-main',
  templateUrl: 'coin-main.component.html',
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoinMainComponent implements OnInit {

  constructor(private readonly store$: Store<RootStoreState.State>) {
  }

  actions: Actions<Coin> = CoinStoreActions.actions;

  ngOnInit() {
  }

  onSelectedItem(value: Asset) {
    const item: Asset = {...value, ...{amount: 0}};
    this.store$.dispatch(CoinStoreActions.CreateRequest({
      item
    }));
  }

}
