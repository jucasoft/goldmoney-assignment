import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {State} from '@root-store/state';
import {RouterStoreSelectors} from '@root-store/router-store/index';
import {map, take} from 'rxjs/operators';
import {evalData} from '@core/utils/j-utils';
import {Subscription} from 'rxjs';
import {Coin} from '@models/vo/coin';
import {HistoryStoreActions, HistoryStoreSelectors} from '@root-store/history-store';
import {SelectItem} from 'primeng/api';


@Component({
  selector: 'app-coin-detail',
  templateUrl: './coin-detail.component.html',
  styles: [``]
})
export class CoinDetailComponent implements OnInit, OnDestroy {

  private subscription: Subscription;

  private item: Coin;

  collection$;

  selectionTypes: SelectItem[] = [
    {label: 'm1', value: 'm1'},
    {label: 'm5', value: 'm5'},
    {label: 'm15', value: 'm15'},
    {label: 'm30', value: 'm30'},
    {label: 'h1', value: 'h1'},
    {label: 'h2', value: 'h2'},
    {label: 'h6', value: 'h6'},
    {label: 'h12', value: 'h12'},
    {label: 'd1', value: 'd1'}
  ];

  constructor(protected store$: Store<State>) {
  }

  cancel() {

  }

  ngOnInit() {
    console.log('CoinEditComponent.ngOnInit()');
    this.subscription = this.store$.select(RouterStoreSelectors.selectExtra).pipe(
      take(1),
    ).subscribe(
      value => this.setState(evalData(() => value.state as Coin, null))
    );

    this.collection$ = this.store$.select(HistoryStoreSelectors.selectAll).pipe(
      map(values => {
        const result = {labels: [], datasets: []};
        const dataset = {
          label: 'History',
          data: [],
          fill: false,
          borderColor: '#4bc0c0'
        };
        values.forEach((item, i, list) => {
          result.labels.push(item.time);
          dataset.data.push(item.priceUsd);
        });
        result.datasets.push(dataset);
        return result;
      })
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  setState(value: Coin): void {
    this.item = value;
  }

  onSelectionType(event) {
    console.log('even');
    this.dispatch(event.value);
  }

  dispatch(interval) {
    this.store$.dispatch(HistoryStoreActions.SearchRequest({path: ['assets', this.item.id, 'history'], queryParams: {interval: interval}}));
  }
}
