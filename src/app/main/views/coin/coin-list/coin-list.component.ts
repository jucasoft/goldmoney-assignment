import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {CoinStoreActions, CoinStoreSelectors, RootStoreState} from '@root-store/index';
import {Observable} from 'rxjs';
import {Coin} from '@models/vo/coin';
import {RouterStoreActions} from '@root-store/router-store/index';
import {ConfirmationService} from 'primeng/api';
import {PopUpData} from '@root-store/router-store/pop-up-base.component';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-coin-list',
  templateUrl: `coin-list.component.html`,
  styles: [`
    /* Column Priorities */
    @media only all {
      th.ui-p-6,
      td.ui-p-6,
      th.ui-p-5,
      td.ui-p-5,
      th.ui-p-4,
      td.ui-p-4,
      th.ui-p-3,
      td.ui-p-3,
      th.ui-p-2,
      td.ui-p-2,
      th.ui-p-1,
      td.ui-p-1 {
        display: none;
      }
    }

    /* Show priority 1 at 320px (20em x 16px) */
    @media screen and (min-width: 20em) {
      th.ui-p-1,
      td.ui-p-1 {
        display: table-cell;
      }
    }

    /* Show priority 2 at 480px (30em x 16px) */
    @media screen and (min-width: 30em) {
      th.ui-p-2,
      td.ui-p-2 {
        display: table-cell;
      }
    }

    /* Show priority 3 at 640px (40em x 16px) */
    @media screen and (min-width: 40em) {
      th.ui-p-3,
      td.ui-p-3 {
        display: table-cell;
      }
    }

    /* Show priority 4 at 800px (50em x 16px) */
    @media screen and (min-width: 50em) {
      th.ui-p-4,
      td.ui-p-4 {
        display: table-cell;
      }
    }

    /* Show priority 5 at 960px (60em x 16px) */
    @media screen and (min-width: 60em) {
      th.ui-p-5,
      td.ui-p-5 {
        display: table-cell;
      }
    }

    /* Show priority 6 at 1,120px (70em x 16px) */
    @media screen and (min-width: 70em) {
      th.ui-p-6,
      td.ui-p-6 {
        display: table-cell;
      }
    }
  `],
  animations: [
    trigger('fadeIn', [
      state('in', style({opacity: '1'})),
      state('out', style({opacity: '0'})),
      transition('* => *', [
        animate(2000)
      ])
    ])
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoinListComponent implements OnInit {

  collection$: Observable<Coin[]>;

  constructor(private store$: Store<RootStoreState.State>,
              private confirmationService: ConfirmationService) {
  }
  aminationState = 'in';

  ngOnInit() {
    this.collection$ = this.store$.select(
      CoinStoreSelectors.selectAllExt
    );

    this.store$.dispatch(
      CoinStoreActions.SearchRequest({queryParams: {}})
    );
  }

  onEdit(item) {
    const stateA: PopUpData<Coin> = {
      item,
      props: {title: 'Edit Coin', route: 'coin'}
    };

    this.store$.dispatch(RouterStoreActions.RouterGo({
      path: ['coin', {outlets: {popUp: ['edit']}}],
      extras: {state: stateA}
    }));
  }

  onDelete(item) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.store$.dispatch(CoinStoreActions.DeleteRequest({item}));
      }
    });
  }

  onDetail(item) {
    this.store$.dispatch(RouterStoreActions.RouterGo({
      path: ['coin', 'detail'],
      extras: {state: item}
    }));
  }

}
