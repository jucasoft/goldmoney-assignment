import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CoinMainComponent} from './coin-main/coin-main.component';
import {CoinListComponent} from './coin-list/coin-list.component';
import {CoinRoutingModule} from './coin-routing.module';
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {PipesModule} from '@core/pipe/pipes.module';
import {SearchModule} from '@components/search/search.module';
import {AssetSelectModule} from '@views/asset/asset-select/asset-select.module';
import {CoinDetailComponent} from '@views/coin/coin-detail/coin-detail.component';
import {CoinEditComponent} from '@views/coin/coin-edit/coin-edit.component';
import {CoinRowComponent} from './coin-row/coin-row.component';
import {PanelModule} from 'primeng/panel';
import {ChartModule} from 'primeng/chart';
import {SelectButtonModule} from 'primeng/selectbutton';

@NgModule({
  declarations: [
    CoinDetailComponent,
    CoinEditComponent,
    CoinMainComponent,
    CoinListComponent,
    CoinRowComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CoinRoutingModule,
    TableModule,
    DialogModule,
    ButtonModule,
    InputTextModule,
    PipesModule,
    SearchModule,
    AssetSelectModule,
    PanelModule,
    ChartModule,
    SelectButtonModule
  ],
  providers: [],
  entryComponents: []
})
export class CoinModule {
}
