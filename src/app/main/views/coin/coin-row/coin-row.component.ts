import {Component, Input, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-coin-row',
  template: `
    <div [@fadeAnimation]="animationState">{{value}}</div>`,
  styles: [``],
  animations: [
    trigger('fadeAnimation', [
      state('in', style({opacity: 1})),
      transition(':enter', [
        style({opacity: 0}),
        animate(600)
      ]),
      transition(':leave',
        animate(600, style({opacity: 0})))
    ])
  ]
})
export class CoinRowComponent implements OnInit {

  @Input()
  public value;

  animationState = null;
  constructor() {
  }

  ngOnInit() {
  }

}
