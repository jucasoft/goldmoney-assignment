import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AssetStoreActions, RootStoreState} from '@root-store/index';
import {Actions} from 'ngrx-entity-crud';
import {Asset} from '@models/vo/asset';

@Component({
  selector: 'app-asset-main',
  templateUrl: 'asset-main.component.html',
  styles: []
})
export class AssetMainComponent implements OnInit {

  constructor(private readonly store$: Store<RootStoreState.State>) {
  }

  actions: Actions<Asset> = AssetStoreActions.actions;

  ngOnInit() {
  }
}
