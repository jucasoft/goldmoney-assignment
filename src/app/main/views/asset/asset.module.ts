import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AssetEditComponent} from './asset-edit/asset-edit.component';
import {AssetMainComponent} from './asset-main/asset-main.component';
import {AssetListComponent} from './asset-list/asset-list.component';
import {AssetRoutingModule} from './asset-routing.module';
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {PipesModule} from '@core/pipe/pipes.module';
import {SearchModule} from '@components/search/search.module';

@NgModule({
  declarations: [
    AssetEditComponent,
    AssetMainComponent,
    AssetListComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AssetRoutingModule,
    TableModule,
    DialogModule,
    ButtonModule,
    InputTextModule,
    PipesModule,
    SearchModule
  ],
  providers: [],
  entryComponents: []
})
export class AssetModule {
}
