import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AssetMainComponent} from './asset-main/asset-main.component';
import {AssetEditComponent} from '@views/asset/asset-edit/asset-edit.component';

const routes: Routes = [
  {
    path: 'main',
    component: AssetMainComponent,
    pathMatch: 'full'
  },
  {
    path: 'edit',
    component: AssetEditComponent,
    outlet: 'popUp',
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'main',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AssetRoutingModule {
}
