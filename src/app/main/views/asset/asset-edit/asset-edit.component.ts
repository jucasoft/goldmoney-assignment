import {Component} from '@angular/core';
import {closePopUpAction, PopUpBaseComponent} from '@root-store/router-store/pop-up-base.component';
import {Asset} from '@models/vo/asset';
import {FormGroup} from '@angular/forms';
import {AssetStoreActions} from '@root-store/asset-store';


@Component({
  selector: 'app-asset-edit',
  templateUrl: './asset-edit.component.html',
  styles: [``]
})
export class AssetEditComponent extends PopUpBaseComponent<Asset> {

  form: FormGroup;
  keys: string[];

  setItemPerform(value: Asset): void {
    const group = this.fb.group({});
    this.keys = Object.keys(value);
    this.keys.forEach(key => group.addControl(key, this.fb.control({value: value[key], disabled: key === 'id'})));
    this.form = group;
  }

  acceptPerform(item: Asset): void {
    if (item.id) {
      this.store$.dispatch(AssetStoreActions.EditRequest({
        item, onResult: [
          // azione che verrà invocata al result della chiamata all'interno dell'effect.
          // chiude la popUP.
          // closePopUpAction: metodo per la creazione dell'azione di chiusura della popUP
          closePopUpAction(this.route)
        ]
      }));
    } else {
      this.store$.dispatch(AssetStoreActions.CreateRequest({
        item, onResult: [
          // azione che verrà invocata al result della chiamata all'interno dell'effect.
          // chiude la popUP.
          // closePopUpAction: metodo per la creazione dell'azione di chiusura della popUP
          closePopUpAction(this.route)
        ]
      }));
    }
  }

  // cancel(): void {
  //   this.store$.dispatch(closePopUpAction(this.route));
  // }
}
