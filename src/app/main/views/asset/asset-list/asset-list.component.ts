import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {AssetStoreActions, AssetStoreSelectors, RootStoreState} from '@root-store/index';
import {Observable} from 'rxjs';
import {Asset} from '@models/vo/asset';
import {RouterStoreActions} from '@root-store/router-store/index';
import {tap} from 'rxjs/operators';
import {ConfirmationService} from 'primeng/api';
import {PopUpData} from '@root-store/router-store/pop-up-base.component';

@Component({
  selector: 'app-asset-list',
  templateUrl: `asset-list.component.html`,
  styles: [``],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AssetListComponent implements OnInit {


  collection$: Observable<Asset[]>;
  cols: any;

  constructor(private store$: Store<RootStoreState.State>,
              private confirmationService: ConfirmationService) {
    console.log('AssetListComponent.constructor()');
  }

  ngOnInit() {
    console.log('AssetListComponent.ngOnInit()');

    this.collection$ = this.store$.select(
      AssetStoreSelectors.selectAll
    ).pipe(
      tap(values => {
        if (values && values.length > 0) {
          this.cols = Object.keys(values[0]);
        }
      })
    );

    this.store$.dispatch(
      AssetStoreActions.SearchRequest({queryParams: {}})
    );

  }

  onEdit(item) {
    console.log('AssetListComponent.onEdit()');

    const state: PopUpData<Asset> = {
      item,
      props: {title: 'Edit Asset', route: 'asset'}
    };

    // apro la popUP
    this.store$.dispatch(RouterStoreActions.RouterGo({
      path: ['asset', {outlets: {popUp: ['edit']}}],
      extras: {state}
    }));

  }

  onCopy(value) {
    console.log('AssetListComponent.onCopy()');

    const item = {...{}, ...value, ...{id: null}};
    const state: PopUpData<Asset> = {
      item,
      props: {title: 'Copy Asset', route: 'asset'}
    };

    this.store$.dispatch(RouterStoreActions.RouterGo({
      path: ['asset', {outlets: {popUp: ['edit']}}],
      extras: {state}
    }));

  }

  onDelete(item) {

    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.store$.dispatch(AssetStoreActions.DeleteRequest({item}));
      }
    });

  }

}
