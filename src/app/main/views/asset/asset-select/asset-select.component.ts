import {ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Store} from '@ngrx/store';
import {AssetStoreActions, AssetStoreSelectors, RootStoreState} from '@root-store/index';
import {Observable, Subject} from 'rxjs';
import {Asset} from '@models/vo/asset';
import {debounceTime, distinctUntilChanged, tap} from 'rxjs/operators';

@Component({
  selector: 'app-asset-select',
  template: `
    <div class="p-grid">
      <div class="p-col">
        <ng-select [items]="collection$ | async"
                   #ngSelect
                   bindLabel="name"
                   autofocus
                   [loading]="loading"
                   [(ngModel)]="selected"
                   [closeOnSelect]="true"
                   (search)="onChange($event)"
                   (clear)="onClear()"
                   pTooltip="Enter the name of the element you want to add."
                   tooltipPosition="left">

          <ng-template ng-typetosearch-tmp>
            <div class="ng-option disabled">
              Start typing...
            </div>
          </ng-template>

          <ng-template ng-label-tmp let-item="item">
            {{item.name}}
          </ng-template>

        </ng-select>
      </div>
      <div class="p-col-fixed" style="width:60px" pTooltip="{{renderToolTipAddButton()}}" tooltipPosition="left">
        <button class="float-right h-100" pButton type="button" label="ADD" [disabled]="!selected" (click)="onAdd($event)"></button>
      </div>
    </div>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AssetSelectComponent implements OnInit {

  collection$: Observable<Asset[]>;
  loading = false;
  input$ = new Subject<string>();
  selected: Asset;

  @Output() selectedItems = new EventEmitter<Asset>();

  constructor(private store$: Store<RootStoreState.State>) {
  }


  ngOnInit() {
    this.collection$ = this.store$.select(
      AssetStoreSelectors.selectAllExt
    );

    this.input$.pipe(
      debounceTime(250),
      // distinctUntilChanged(),
      tap(search => {
        console.log('dispach');
        this.store$.dispatch(
          AssetStoreActions.SearchRequest({
            queryParams: {search}
          })
        );
      })
    ).subscribe();
  }

  renderToolTipAddButton(): string {
    let result = '';
    if (!this.selected) {
      result = 'Button disabled, there are no selected item.';
    } else {
      result = 'Add selected item.';
    }
    return result;
  }

  onAdd($event: MouseEvent) {
    this.selectedItems.emit(this.selected);
    this.onClear();
  }

  onChange($event: any) {
    this.input$.next($event.term);
  }

  onClear() {
    this.selected = null;
    this.store$.dispatch(
      AssetStoreActions.Reset()
    );
  }
}
