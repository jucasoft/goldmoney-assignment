import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AssetSelectComponent} from '@views/asset/asset-select/asset-select.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {FormsModule} from '@angular/forms';
import {TooltipModule} from 'primeng/tooltip';
import {ButtonModule} from 'primeng/button';

@NgModule({
  declarations: [AssetSelectComponent],
  imports: [
    CommonModule,
    NgSelectModule,
    FormsModule,
    TooltipModule,
    ButtonModule
  ],
  exports: [AssetSelectComponent]
})
export class AssetSelectModule {
}
