import {Injectable} from '@angular/core';
import {Asset} from '@models/vo/asset';
import {environment} from '../../../environments/environment';
import {BaseCrudService} from 'ngrx-entity-crud';

@Injectable({
  providedIn: 'root'
})
export class AssetService extends BaseCrudService<Asset> {
  protected service = environment.webServicecoincapUri + 'assets';
}
