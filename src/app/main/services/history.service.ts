import {Injectable} from '@angular/core';
import {History} from '@models/vo/history';
import {environment} from '../../../environments/environment';
import {BaseCrudService} from 'ngrx-entity-crud';

@Injectable({
	providedIn: 'root'
})
export class HistoryService extends BaseCrudService<History> {
  protected service = environment.webServicecoincapUri;
}
