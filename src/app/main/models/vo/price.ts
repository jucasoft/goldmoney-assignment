export class Price {
  public id: string = undefined;
  public value: any = undefined;

  /**
   * metodo statico utilizzato per recuperare l'id dell'entita.
   * @param item
   */
  static selectId: (item: Price) => string = item => item.id;
}
