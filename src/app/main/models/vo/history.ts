export class History {
    public date: string = undefined;
    public priceUsd: string = undefined;
    public time: number = undefined;
    /**
     * metodo statico utilizzato per recuperare l'id dell'entita.
     * @param item
     */
    static selectId: (item: History) => string = item => item.date;
}
