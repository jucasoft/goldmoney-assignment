export class Asset {
  public id: string = undefined;
  public disabled = false;
  /**
   * metodo statico utilizzato per recuperare l'id dell'entita.
   * @param item
   */
  static selectId: (item: Asset) => string = item => item.id;
}
